app.controller('depotStockCtrl', ['$scope', 'factory', '$location', '$http', '$timeout', '$interval', '$window', 'fileUpload', 'ngToast', 'jwtHelper',
  function ($scope, factory, $location, $http, $timeout, $interval, $window, fileUpload, ngToast, jwtHelper) {

    $scope.auth = true;
    factory.validatePath();
    $scope.token = JSON.parse(window.localStorage.getItem("token"));

    function callAtTimeout() {
      ngToast.create({
        className: "danger",
        content: "<b>Session Timed out, Please login again.</b>",
        timeout: 2500
      });
      localStorage.clear();
      $location.path("/");
    }

    if (!$scope.token) {
      callAtTimeout();
    }

    $scope.decodedToken = jwtHelper.decodeToken($scope.token);

    if ($scope.decodedToken.exp <= Date.now()) {
      callAtTimeout();
    }

    $scope.profile = JSON.parse(window.localStorage.getItem("profile"));
    $http.defaults.headers.common["token"] = $scope.token;

    $scope.emailValidator = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;
    $scope.pincode = /^[0-9]{1,6}$/;
    $scope.numbers = /^[1-9]\d*(\.\d+)?$/;
    $scope.mobileNumber = /^[789]\d{9}$/;
    $scope.telNumber = /^[0-9]\d{2,4}-\d{6,8}$/;
    $scope.models = [];
    $scope.allStock = [];
    $scope.modelCode = "Select";
    $scope.variantCode = "Select";
    $scope.variants = [];
    $scope.date = new Date();
    // Role menus
    var menus = JSON.parse(window.localStorage.getItem("userMenu"));
    var result = menus.roleMenus;
    $scope.add = result[0].depot_stock_sub.add;
    $scope.add = result[0].depot_stock_sub.add;
    $scope.edit = result[0].depot_stock_sub.edit;
    $scope.delete = result[0].depot_stock_sub.delete;
    $scope.modalAdd = true;
    if (!$scope.add) {
      $scope.modalAdd = false;
    }
    $scope.modalEdit = true;
    if (!$scope.edit) {
      $scope.modalEdit = false;
    }
    $scope.modalDelete = true;
    if (!$scope.delete) {
      $scope.modalDelete = false;
    }
    $scope.depotStock = {
      plant: PlantInfo
    };

    $scope.selectedItem = {
      depot: "",
      model: "",
      variant: ""
    };
    $scope.getDepots = function () {
      var match = {};
      var dataIp = factory.queryForm(match);
      $scope.depots = [];
      $scope.depoDetails = [];
      $http.get('/viewDepo').success(function (data) {
        if (data.status == "true") {
          $scope.depots = data.data;

        }
      })
    }();


    $scope.getVariants = function (model) {
      $http.get('/variant/model/variants/' + model).success(function (data) {
        if (data.status == "true") {
          $scope.variants = data.data;
          return;
        }
        $scope.errorMessage = data.data;
      }).error(function (error) {
        ngToast.create({
          className: "danger",
          content: "<b>Network error, Please login again.</b>",
          timeout: 3500
        });
        return;
      });
    };
    $scope.showData = function () {
      var query = {};
      if ($scope.depot) {
        query.depot = $scope.depot;
      }
      if ($scope.model) {
        query.model = $scope.model;
      }
      if ($scope.variant)
        query.variant = $scope.variant.variantCode;
      $http.post('/vehicle/getFilteredDepotStock', query).success(function (data) {
        $scope.depotStockList = data.data;
      })
    }
    $scope.clear = function () {
      $scope.depot = "";
      $scope.model = "";
      $scope.variant = {};
      var match = {
        type: "DEPOTSTOCK"
      }
      match.stock = true;
      var data = factory.queryForm(match);
      $http.post('/vehicle/getVehicles', data).success(function (data) {
        $scope.depotStockList = data.data;
      })
    }

    //depot stock upload from excel
    $scope.uploadDepotStock = function () {

      var file = $scope.myFile;
      var uploadUrl = "/vehicle/depotStockUploadFromPlant";
      fileUpload.uploadFileToUrl(file, uploadUrl, function (data) {
        if (data.status == "true") {
          ngToast.create({
            type: 'slide',
            className: 'success',
            content: '<b>Vehicles added successfully</b>',
            timeout: 4000
          });
          //$scope.successMessage = data.msg;
          // $scope.successMessage = true;
          for (var i = 0; i < data.length; i++) {
            $scope.depotStockList.push(data[i]);
          }
          $window.location.reload();
          // $scope.getStock();
          return;
        } else {
          ngToast.create({
            type: 'slide',
            className: 'danger',
            content: '<b>Error while uploading file. Please try again</b>',
            timeout: 3500
          });
          return;
          //$scope.errorMessage = data.msg;
        }
      });

    };


    //dealer stock upload

    $scope.uploadDealerStock = function () {
      alert(' asdsj kasnd kjn');
      var file = $scope.myFile1;
      var uploadUrl = "/vehicle/dealerStockUploadFromPlant";
      fileUpload.uploadFileToUrl(file, uploadUrl, function (data) {
        if (data.status == "true") {
          ngToast.create({
            type: 'slide',
            className: 'success',
            content: '<b>Vehicles added successfully</b>',
            timeout: 4000
          });
          //$scope.successMessage = data.msg;
          // $scope.successMessage = true;
          for (var i = 0; i < data.length; i++) {
            $scope.depotStockList.push(data[i]);
          }
          $window.location.reload();
          // $scope.getStock();
          return;
        } else {
          ngToast.create({
            type: 'slide',
            className: 'danger',
            content: '<b>Error while uploading file. Please try again</b>',
            timeout: 3500
          });
          return;
          //$scope.errorMessage = data.msg;
        }
      });

    };

    $scope.uploadAdvanceStock = function () {
      alert(' advacne');
      var file = $scope.myFile2;
      var uploadUrl = "/vehicle/advancestockUpload";
      fileUpload.uploadFileToUrl(file, uploadUrl, function (data) {
        if (data.status == "true") {
          ngToast.create({
            type: 'slide',
            className: 'success',
            content: '<b>Vehicles added successfully</b>',
            timeout: 4000
          });
          //$scope.successMessage = data.msg;
          // $scope.successMessage = true;
          for (var i = 0; i < data.length; i++) {
            //  $scope.depotStockList.push(data[i]);
          }
          $window.location.reload();
          // $scope.getStock();
          return;
        } else {
          ngToast.create({
            type: 'slide',
            className: 'danger',
            content: '<b>Error while uploading file. Please try again</b>',
            timeout: 3500
          });
          return;
          //$scope.errorMessage = data.msg;
        }
      });

    };

    // Pagination
    $scope.pageSize = 10;
    $scope.currentPage = 1;
    $scope.pagelengths = [10, 25, 50, 100];
    $scope.limits = [{
      key: 10,
      value: 10
    }, {
      key: 50,
      value: 50
    }, {
      key: 100,
      value: 100
    }];


    $scope.depotStockList = [];
    $scope.viewDepotStock = {};
    $scope.editDepotStock = {};
    $scope.deleteDepotStock = {};
    $scope.errorMessage = "";
    $scope.successMessage = "";
    var editItem = {};

    $scope.loading = true;
    $scope.getStock = function (depotCode) {
      var match = {
        type: "DEPOTSTOCK"
      }
      if (depotCode) {
        match.locationCode = depotCode;
        match.stock = true;
      }
      var data = factory.queryForm(match);
      $http.post('/vehicle/getVehicles', data).success(function (data) {
        $scope.depotStockList = data.data;
        $scope.limits.push({
          key: 'ALL',
          value: $scope.depotStockList.length
        });
        $scope.loading = false;


      }).error(function (error) {
        $scope.errorMessage = error;
      });
    };

    if ((localStorage.getItem('depotId') !== null || localStorage.getItem('depotId') !== undefined)) {
      $scope.depotId = JSON.parse(localStorage.getItem('depotId'));
      $scope.depotName = JSON.parse(localStorage.getItem('depot'));
      localStorage.removeItem('depotId');
      localStorage.removeItem('depot');
      $scope.getStock($scope.depotId);
    } else {
      $scope.depotId = "";
      $scope.getStock($scope.depotId);
    }


    function shuffleSentence(words) {
      // words = words || faker.lorem.words(8);
      var str = words.trim();
      return str.charAt(0).toUpperCase() + str.slice(1);
    }

    $scope.form22 = function (vehicle) {
      $http.get('/variant/variantDetails/' + vehicle.variantCode).success(function (data) {
        if (data.status == "true") {
          $scope.variant = data.data;
          var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gAgQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3MA/9sAhAADAwMDAwMEBAQEBQUFBQUHBwYGBwcLCAkICQgLEQsMCwsMCxEPEg8ODxIPGxUTExUbHxoZGh8mIiImMC0wPj5UAQMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlT/wgARCABIAJQDASIAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAABwAEBQYIAgMBCf/aAAgBAQAAAAD9Uw+NziMreVUkkkkkgkPbOMyLVTCVwjVTuOqlNmiMB60dm7nIu6ML7HrOosq65+Zm0BDZ60APD4hmADYXwyID4RwNRNMALRr7JZqBBxKGO73jjeeCtbWE98DSkROjYfNugKAd1n+EYxtuhb8Zc08HEZ1eWN7Kkl5cgDwtIXJ54SSSSSSgojjzct3UVJtHVqHczKUx7Y7H50+YZ00iw3q15ohcrzpz98Jl/wD/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAQUCBgcEA//aAAgBAhAAAACZY6Pc0m16fvf2T5+N3248uterZS+XLPHZ+m326U4gAB//xAAaAQEAAgMBAAAAAAAAAAAAAAAABQYCAwcE/9oACAEDEAAAAAtsXK12zVDA29Ph6v0GP50MuhenwaI2tAAAH//EACsQAAEFAQABAwMEAgMBAAAAAAQBAgMFBgcACBITERQXEBU2UzAzISIxNf/aAAgBAQABDADzZd4yeWJlCEZJalw+p0j5kWbNx/Hh+o5beIsQMr4DNh3K/wAboJqw/LRN8xHbcpsCowZEfWnf5e9bonNZ+Crr5XRF895llaPLM2G2ViwjdM4ffTMqSKOMaDqnOpebWQOhzs8sYMPded3YAEWjppZyOncvzzs8zZY16fZcR25Gwyr4TZFkPVUTzR9ZJjJkGpI4kjZ0vcQqkjykczF31hoaFhxsMcUm432iodISCHJCkP5X1/8AaP5Xdktonoh4I8zM/pKnTC/cAyqq7a2Mz+dnODcxJ/yvr/7R/Pyvr/7R/KKQ+emBlPVqk9lxOg3HRKcGvYnw+pE8KKnpKgYuFHeX1Da2HBIwiopHnecginF41oZbL/gP0yJMlnoXp9fi1CTJmrdB/wDZkSqwLRAT2TGvFRwpgyK1Y5oRxxxIWwQRMii6l/MzfM4KI/M03ugicuo51SXY0jxR4xDM/cG5S/jn/wCzF6d7FxJLmL9W8dhjms7NHsa7z7QX+iL9Ov7x+EzSOEVP3HnHHydvDJo9MYRCFls1w4K1ijqH1JNh5oc5wWutJiLZtTCZ1HrgumAZl8qO9lfx3CzYjKowxPoeqI5FRU+qbDlxoc0plKxSB6LVX+WIVBpntZktWFqa/wCaBvxzdS/mZvmW/jFL+m3Ria239n/m2934yH93191E/Rslm/ZUNWTm7NY8o0i8ksUj89S00y6uog+i/F30kmgwtJU1j3RgxySQyNkjc5j7fXW83DlvhpXIeqq5VVVVV4DU0guUttMTWxvM470C+3mq0RJv0YP4IYIfA2cWeOaLrFNUOpHWT0ZGbx55DdEUjP8AX1L+Zm+Zb+MUvmm19VmBXullZKTR1huu0TIV+r39PaxmKJYxPa3jH/1bP9fUNjiLejDvA2K+TEbTIdFx0WQ1szICr703sDpzy6q2nNJ450inBrJ8dqPYwKb05ZQsj70O+Ijruk7zKZbIrici+OVOB44jNZaayNYsRNkOQXWljjvSOZ+K39BM5RISm+Nx2/0E7VKHLcuLyA2TBeiyJKTv8jpLjUFFhV8k0LMb0FGNY0QxGVvJdMZL9TXwBsz2XqsuIsAbVV3Qa064zBAoULppuY5u8oTz5LAN47f0VEcioqIqbP071FuVKZQFpWyc55f0jAXbCEtamQDfcQz2xIlPDl/a7B3pt26PdEy1qVhxPp+pM8VEfclJZkf5drpJcrR/uEUUMjo+jUo3sgsnrGRJ0zNjSRxvgtUkTpdR9wx6/I8aDo+aKJrhxkMnfc7G4pNDMKUNWx1gfRs0egiCIcRNR9UqC6mMixiJgKh6VlXoJ7ZSfK7pdUgIT7T3xTjdIzEpj4PcU1ALkOyqn2ADZJ4arpyk1wh57K8aKDpWZmYnwssJCKHa56+tC6sGd7iTenWAAs/3FSIIRD0GzOJoECrRHx42/uNBDYSniCQM8sqgS2ggiJRytP5xmj7c+ymQr5psnSuK+6ckvywYHOiFCEsbOkjOaWNfsK+zrJRGB3OWprx5Uh8T5PBue1AKiywG2MUzMDnYFC9rJ/H82zfzxyNUtifjDOK6P6yn+wDmeuArbwJstRI2pzolJXfYQEE/D+Ms7I0lSZDipiOe0cxLCopjhSqXHVGfsjDAnTotlja00c+JCTRkBwtLUWX34MxgrqWgCzwzBxZSFg8//8QAOhAAAgEDAgIHBAcIAwAAAAAAAQIDAAQREiETMQUQFCJBUWEyQoKhFSMwcnOBszNDUlNicZKykbHB/9oACAEBAA0/AKiOHWFgsSEcwZDnf+wNf03Z1fNKVcvZz4WTA5lfBhUZDRt2wkSxHkytw/GpMCOGcgpIfJJBzPocfbdKFwZVOCkCe3gjkWJxTxiWC3lzw0jb2Cyjd3fwWnOhJriwhSEavJkJZKkuFMJDFmtZh3lAbxU+FGALclrSKaFH5NjU2cGsCWaGJiU0E44ked10n2lrox1hmkO7SIwzG59TgjqRiDcyDUXI8VHICidg1ugX5AU8rhTHkBlXbVg+uRUaRlQ0YJ7ygmvwRXiYsxt8ywpcCSJhh4z6ikkiALLqHeYA1+CK/BFSQK8ukaQC4zjHpX0SpknfIjhAlfUzmoLgvJZq+XVRHiNivVB0RbTaCCXXgFZMeeoIMdUov3iD8jFwArfkSDQtrYN94s2muwz4xz9g0knf1DIBIIViPIGpF2xhlYGlzpRFCqN87AVw4P0xTdG22olAecYoKSkkY0KzeTgbEGoZTHcx+ag4dSKaSAg+YLihbJzAPvV9wdV+zQ2mfcAGXl+GpmaRSW+uuMHvSM750rSHuarvtDll8UV2YZHp1By8sIunBDesEbY+VMUR3SMxmYKe7FFGNwlXzie5X+XthI/hFHmKJLNbj9pH6KPeFK/1ltJkoSOYKnkajwJ4SclCfH1Brhwfpivo21/THV2p/wDnxrgWOc/DRQcXsqux0+uikjVIornWoZmOSwD+QHUnRYdPvPK4b/UVNIsEhQ7NHDH3E/saRgyspwQRuCCKk6KhDSpzDs6xSuPIjJNE5JNWNzcCO5c7iOKFXITOy8+dC2t+BbqSUhAdsD1Y53PU3J0IYfKo5EWNhgGUMcFD54G9GxYyeWzriuHB+mK+jbX9MVpPCtg2WZvAt5LVzO0txJ5KTl2NLJAAPIBxXZk/26+jNazooyTA/v8AwGoESOGV3EfE4YxG6OdhIBsQedRxGS2tjEo4mNyuoHckcqlMqwSTfs1EvtwyeQJJINP3wgCSYTx0y08ZhuZo21oiN7YL+9I/jXSzpIEYYKwoDw8+pyTU0EiRu3JSwwCcV/NtJva/xIND95dyEBR8ZzU5BmlxgHHJV/pFOkQDgqOSAHmawAFE4Ax/lXNiziV/yCZFPgyzPu8hH/g8qaSIqgIGcMCedSwKqEkbkN6HrIwQakOWtnUvBk/w43SpcLdWwlmbWmeagxgBhT5MjxpqilPm6bb+oosOckwJ9SvDIqIhkhCaIEYciQcl/tjdW0I4zska8eQJqYqGIC5ycA0OjWvneGOR4GjSLjtwnZVLYTflT8ICMWExYGfPCBAXZpMd0VcW9s1tHHBM9y0k0ksehowvnCRV8bcIyQMFjNxnQJGbAU4QnHpUFk19LdtcSmQW6MEb6sRkF8nYZq4eRezxWsjyxcIgO0igd1RqG9BIi0aWspEnGlMKcHYlwWGNquAC57PJi2zKYBxzjEeZFK1cF2kMMLvFBH2hreN5WGQgdlwCaWSNOO8DJEeLN2dGDH3C4K6uVLxhGApUyGFih0g4zkrsfGnluu0xRTSzTQLb2r3DI6GNSsq6DqB28qM4iS0FnL2hsxibWI8Z06DnNWyuzho2QMI34blCeYVtjSdMS2IFzfCOBBFbLc5kmCEBmDYUeddN2qy28Zum7RHqiLl5I1jIESsNJbNW949vDJbTPMkxiOmRlLoh0htgeqC7t7lNJx9ZbyCRPyyKvoZkmUS4QceDs7kbZBKVxrKXZ9tVkDwv+96tWjMWZPGKSWUfOZqtBbRxmbEkqQRljIgVom77lzhw6kVcdHyWUg1YBidg5x5NkbGrfWvHScK8kchUmN8KAVyoqyW2EWZOQtp+0pn46Dlpo0nIS4HaGugJh7wWRyRSnEkXaCEmjExnWOQDmiuxIrpeJ45mcnOuR2czqVhVwUzlYyzDPvULSGBV4mBGIk0ao8eyzcyfE1cmbjTzTZkcS272uCQByjcgVGYtFxBOUkURxCHSNiMMo3q6Ll0dgyKZH1uV2z3m351f3purgwTaC7mNYiM4OFwoowW8HCjm7hjtk0Rpgg7AVHBFEkUkmtRwyx1AfxMW7x8er//EAC4RAAIBBAEDAgUCBwAAAAAAAAECAwAEBREGEiFBFFIHEBMiMReRIDBRYZOi0v/aAAgBAgEBPwACtCtCj2q6+IXH7W4aEGefpOjJEgKfuSKvuSY7H4q3yUolME5QJpPu+8EjYOqh+InG5XCs88Q9zx9v9SalyVnHjpL9ZBLbpE0nUmm2qjZ1X6k8e9l1/jH/AFUMgmijkCsodQ2mGiNjejQomt7q5i9RBNF1FfqIy7HjY1WEyt1weabH5Sxb6MspYTIN78bHuWufXNrd8VtJrV1eF7qMoV/GulqTG2GRxNvDc28ciNboO6jY+38g+DXH5JbfFcrxvWXit4Zin9iAyn99VwTOILWzxfoJ3ZnkP1+kdAGy35+XgUe9E77CpWKI7KpYhSQo868VleR5LK42bHTccuvUSoVG0YqreHHbxWUwGTs+D2lmYZJbj1okaONS5QMG9tLybks1nHaWOAuY5hGqCaUEKuhrfcAVYcYnw3F8ssm5r27tpDIE23fpOlH9T3rg1vPa8ct454nicPLtHUqRtj4PyGxWzWz/AA6/m//EAC4RAAIBAwMBBQcFAAAAAAAAAAECAwQFEQAGIRITIDFBURQWIlJhkaEjQEKS0f/aAAgBAwEBPwDuU+y71UQrIexi6hkJIxDfgHVJYq6tuM1BGYxNCG68tx8JweRqXZN8jQsqwyfRX5/ONJQ1L1yUbIY5mkEfS/GCTjnXuLevmp/7n/NSoYpHQkHpYjI8DjuQydjNHJjPQ6tj1wdXW3U+7Yoq231Y7WOPBiY+Hnz8p1s2Coptw1EVQrLKtO/WG8c9S6avrKG5TSwTOjLM54P189XlI57jt2u6Qsk0sXV91YfbOt32ljU1Nw9siUBU/Rz8Z4C91AGdQWCgkAk+WrdY6G3V8VbFfKfsI2DHDAMR8p589W+8UFVu2pqhKkcPspRXchAxBX101hsUVU9TV3mB4i5cxxkZOTnHBJ1WX+G6bgtpTEVLTToELcfyGWPprds0NRfJ5IpEkQrHhlIYcKPMftv/2Q==';
          var doc = new jsPDF('p', 'pt');
          var lMargin = 15; //left margin in mm
          var rMargin = 15; //right margin in mm
          var pdfInMM = 210; // width of A4 in mm
          doc.setFont('times');
          doc.setFontSize(13);
          doc.addImage(imgData, 'JPG', 10, 15, 120, 50);
          // doc.setFontStyle('bold');
          var title = "FORM 22",
            xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
          doc.text(title, xOffset, 50);
          var title1 = "See rules 47 (1)(g), 115, 124 (2) and 127";
          xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title1) * doc.internal.getFontSize() / 2);
          doc.text(title1, xOffset, 70);
          var title2 = "INITIAL CERTIFICATE OF COMPILANCE WITH POLLUTION STANDARDS, ";
          xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title2) * doc.internal.getFontSize() / 2);
          doc.text(title2, xOffset, 90);

          var title3 = "SAFETY STANDARDS OF COMPONENTS AND ROAD WORTHINESS";
          xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title3) * doc.internal.getFontSize() / 2);
          doc.text(title3, xOffset, 105);


          var data = "It is certify that the following vehicle compiles with the emission values, including mass emission norms and noise" + " " +
            "standards including noise level under provisions of the Motor Vehicle Act 1998, and the rules made there under as specified below";
          var text = doc.splitTextToSize(shuffleSentence(data) + '.', doc.internal.pageSize.width - 80, {});
          doc.text(text, 40, 130);
          var data1 = [];
          var cols1 = [{
              title: "No of Working Days",
              dataKey: "key"
            },
            {
              title: "No of Days Present",
              dataKey: "value"
            }

          ];
          data1.push({
            key: "1. Brand name of the vehicle   : ",
            value: vehicle.model

          });
          data1.push({
            key: "2. Chasis Number  :",
            value: vehicle.chassisNo

          });
          data1.push({
            key: "3. Engine Number  : ",
            value: vehicle.engineNo

          });
          data1.push({
            key: "4.  Emission norms  : ",
            value: "Bharat (Trem) Stage -III A"

          });


          doc.autoTable(cols1, data1, {
            startY: 180,
            styles: {
              font: 'times',
              halign: "left"
            },
            drawHeaderRow: function () {
              // Don't draw header row
              return false;
            },
            columnStyles: {
              days: {
                fillColor: [41, 128, 185],
                textColor: 255,
                fontStyle: 'bold'
              }
            }
          });

          var point = "5. The emission, sound level for horn and pass by  noise values of the above vehicle model, obtained during the approval as per " +
            " central Motor Vehicle Rules, 1989 are below:";
          var text1 = doc.splitTextToSize(shuffleSentence(point) + '.', doc.internal.pageSize.width - 80, {});
          doc.text(text1, 40, 280);

          doc.text("(i) Emission values [refer rule 115(2)] :", 40, 330);
          doc.text("(b) For diesel vehicles :", 40, 350);

          var data = [];
          var cols = [{
              title: "Sr No",
              dataKey: "no"
            },
            {
              title: "Pollutant",
              dataKey: "poll"
            },
            {
              title: "Mass gram/kilowatt per hour",
              dataKey: "mass"
            }
          ];
          if (isNaN($scope.variant.NonMethaneHC)) {
            $scope.variant.NonMethaneHC = 0;
          }
          if (isNaN($scope.variant.carbonMonoxide)) {
            $scope.variant.carbonMonoxide = 0;
          }
          if (isNaN($scope.variant.hydroCarbon)) {
            $scope.variant.hydroCarbon = 0;
          }
          if (isNaN($scope.variant.Nox)) {
            $scope.variant.Nox = 0;
          }
          data.push({
            no: '1',
            poll: 'Carbon Monoxide',
            mass: $scope.variant.carbonMonoxide

          });
          data.push({
            no: '2',
            poll: 'Hydro Carbon',
            mass: $scope.variant.hydroCarbon

          });
          data.push({
            no: '3',
            poll: 'Non-Methane HC',
            mass: $scope.variant.NonMethaneHC

          });
          data.push({
            no: '4',
            poll: 'Nox if applicable',
            mass: $scope.variant.Nox

          });

          data.push({
            no: '5',
            poll: 'HC + Nox if applicable',
            mass: (parseFloat($scope.variant.hydroCarbon) + parseFloat($scope.variant.Nox)).toFixed(2)
          });
          data.push({
            no: '6',
            poll: 'PM',
            mass: $scope.variant.PMv

          });
          doc.autoTable(cols, data, {
            startY: 360,
            styles: {
              font: 'times',
              halign: "center",
              lineColor: [44, 62, 80],
              lineWidth: 1
            },
            headerStyles: {
              fillColor: 255,
              fontSize: 15,
              rowHeight: 30,
              textColor: 20
            }
          });
          doc.text("(ii) Noise level [refer rule 119 and 120] :", 40, 530);
          var pointa = "(a) horn (for all vehicles other than agricultural tractors and construction equipment vehicles) as installed on the vehicle: ............dB(A) - Not Applicable";
          var texta = doc.splitTextToSize(shuffleSentence(pointa) + '.', doc.internal.pageSize.width - 80, {});
          var pointb = "(b) Bystander's position (for all vehicles other than agricultural tractors and construction equipment vehicles)............dB(A) - Not Applicable";
          var textb = doc.splitTextToSize(shuffleSentence(pointb) + '.', doc.internal.pageSize.width - 80, {});

          var pointc = "(c) Operator's ear level (for agricultural tractors and construction equipment vehicles)............96 dB(A) Max Compiles with the provisions";
          var textc = doc.splitTextToSize(shuffleSentence(pointc) + '.', doc.internal.pageSize.width - 80, {});

          doc.text(texta, 40, 550);
          doc.text(textb, 40, 590);
          doc.text(textc, 40, 630);
          doc.text("Same Deutz-Fahr India (P) Limited", 360, 690);
          doc.text("(Signature of Manufacturer)", 360, 720);

          doc.setFontSize(9);
          var title = "SAME DEUTZ-FAHR INDIA (P) LTD. CIN. No. U40105TN1999PTC043776";
          doc.text(title, 40, 740);
          var title1 = "72 M, Sipcot Industrial Complex, Ranipet – 632 403, Tamilnadu, India";
          doc.text(title1, 40, 760);
          var title2 = "Tel. : +91-4172-247156 to 163 Fax : - 91-4172-246330/244981";
          doc.text(title2, 40, 780);
          var title3 = "indiareception@sdindia.com   www.samedeutz-fahr.com";
          doc.text(title3, 40, 800);
          var blob = doc.output("blob");
          window.open(URL.createObjectURL(blob));
          // var name = vehicle.serialNo + "form_22";
          // doc.save(name);
        }
        $scope.errorMessage = data.data;
      }).error(function (error) {
        $scope.errorMessage = error;
      });

    };


    $scope.updateDepotFun = function () {
      $scope.updateErrorMessage = "";
      $scope.updateSuccessMessage = "";
      $scope.editDepotStock.depotStockCreatedBy = $scope.profile.id;
      $scope.editDepotStock.action = "UPDATE";
      //$scope.updateDepotStock.submitted = true;

      if ($scope.updateDepotStock.$valid) {
        $http.post('/vehicle/updateVehicleDetails', $scope.editDepotStock).success(function (data) {
          if (data.status == "true") {
            $scope.updateSuccessMessage = data.msg;
            editItem = $scope.editDepotStock;
            // $scope.UpdateDepotStock.submitted = false;
            $timeout(function () {
              $('#UpdateDepotStock').hide();
              $('#updateSuccessMessage').hide();
            }, 2000);
            return;
          }
          $scope.updateErrorMessage = data.msg;
        }).error(function (error) {
          $scope.updateErrorMessage = error.msg;
        });
      }
    };
    $scope.getModels = function () {
      $http.get('/variant/models').success(function (data) {
        if (data.status == "true") {
          $scope.model = data.data;
          return;
        }
        $scope.errorMessage = data.data;
      }).error(function (error) {
        ngToast.create({
          className: "danger",
          content: "<b>Network error, Please login again.</b>",
          timeout: 3500
        });
        return;
      });
    }
    $scope.getModels();
    $scope.editDepotIndo = function (stockInfo) {
      if (!$scope.modalEdit) {
        ngToast.create({
          className: "danger",
          content: "<b>You are not authorized to perform this action. </b>",
          timeout: 2500
        });
        return;
      } else {
        $("#UpdateDepotStock").modal("show");
        editItem = angular.copy(stockInfo);
        $scope.editDepotStock = editItem;
        $scope.getVariants($scope.editDepotStock.model);
        // $scope.getVariantDetails($scope.editDepotStock.variantCode);
      }
    };

    $scope.closeEdit = function () {
      $scope.errorMessage = "";
      $scope.successMessage = "";
      $scope.depotStockList[$scope.editDepotStock.index] = editItem;
    };

    $scope.viewFullInfo = function (index) {
      $scope.viewDepotStock = $scope.depotStockList[index];
    };

    $scope.createNewDepotStock = function (depotStock) {
      $scope.errorMessage = "";
      $scope.successMessage = "";
      $scope.depotStock.depotStockCreatedBy = $scope.profile.id;
      $scope.depotStock;

      $scope.createDepotStock.submitted = true;
      if ($scope.createDepotStock.$valid) {
        $http.post("/vehicle/addVehicle", $scope.depotStock).success(function (data) {
            if (data.status == true) {
              $scope.successMessage = data.message;
              $scope.depotStockList.push(data.data);
              $scope.depotStock = {};
              $scope.createDepotStock.submitted = false;
              return;
            }
            $scope.errorMessage = data.msg;
          })
          .error(function (error) {
            $scope.errorMessage = error;
          });
      }
    };

    $scope.uploadFile = function (file) {
      var file = $scope.myFile;
      var uploadUrl = "/depotStockUpload";
      fileUpload.uploadFileToUrl(file, uploadUrl, function (data) {
        if (data.status == "true") {
          $scope.successMessage = data.msg;
          $scope.viewDepotStocksList();
          $scope.notUploaded = data.notUploaded;

          for (var i = 0; i < data.data.length; i++) {
            $scope.depotStockList.push(data.data[i]);

            $timeout(function () {
              $("#successMessage").hide();
            }, 2000);
          }
          return;
        } else {
          $scope.errorMessage = data.msg;
          $scope.notUploaded = data.data;
        }
      });
    };

    // $scope.upload = function (file) {
    //     Upload.upload({
    //         url: '/upload1',
    //         data: {file: file}
    //     }).success(function (resp) {
    //       console.log(resp)
    //     })
    // };

    $scope.deleteDepotIndo = function (chasisNo) {
      $scope.deleteErrorMessage = "";
      $scope.deleteSuccessMessage = "";
      $scope.deleteDepotStock.chasisNo = chasisNo;
      $http.post('/vehicle/deleteDepotStock', $scope.deleteDepotStock).success(function (data) {
        if (data.status == "true") {
          $scope.deleteSuccessMessage = data.msg;
          return;
        }
        $scope.deleteErrorMessage = data.msg;
      }).error(function (error) {
        $scope.deleteErrorMessage = error.msg;
      });
    };
    $scope.updateDepotFun = function () {
      $scope.updateErrorMessage = "";
      $scope.updateSuccessMessage = "";
      $scope.editDepotStock.depotStockCreatedBy = $scope.profile.id;
      $scope.editDepotStock.action = "UPDATE";
      if ($scope.updateDepotStock.$valid) {

        $http.post('/vehicle/changeVariantValue', $scope.editDepotStock).success(function (data) {
          if (data.status == true || data.status == 'true') {
            $scope.showData();
            ngToast.create({
              type: 'slide',
              className: 'success',
              content: '<b>Vehicle has been updated successfully</b>',
              timeout: 4000
            });
            // $scope.updateSuccessMessage = data.msg;
            $timeout(function () {
              $('#UpdateDepotStock').modal('hide');
              $('.modal-backdrop').remove();
            }, 2000);
            editItem = $scope.editDepotStock;
            //$scope.editDepotStock = {};
            // $scope.UpdateDepotStock.submitted = false;
            return;
          }
          // $scope.updateErrorMessage = data.msg;
          ngToast.create({
            type: 'slide',
            className: 'danger',
            content: '<b>Unable to update the vehicle.</b>',
            timeout: 3500
          });
        }).error(function (error) {
          // $scope.updateErrorMessage = error.msg;
          ngToast.create({
            type: 'slide',
            className: 'danger',
            content: '<b>Some Technical Error. Please try after some time.</b>',
            timeout: 3500
          });
        });
      }
    };
    $scope.download = function () {
      var wb = XLSX.utils.table_to_book(document.getElementById('depotstock'), {
        display: false,
        raw: true
      });
      XLSX.writeFile(wb, "depotstock.xlsx");
    }
    $scope.showLoading = function () {
      $scope.loading = true;
      $timeout(function () {
        $scope.loading = false;
      }, 2000);
      return;
    }
  }
]);