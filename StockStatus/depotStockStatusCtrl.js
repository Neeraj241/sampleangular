/**
 * @Author: vinod
 * @Date:   2018-03-06T17:57:34+05:30
 * @Last modified by:   vinod
 * @Last modified time: 2018-03-22T16:55:59+05:30
 */



app.controller('stockStatusCtrl', ['$scope', 'factory', '$location', '$http', '$timeout', '$interval', function($scope, factory, $location, $http, $timeout, $interval) {
  function callAtTimeout() {
    window.localStorage.removeItem('profile');
    window.localStorage.removeItem('token');
    $location.path('/');

  }
    factory.validatePath();
  $scope.auth = true;
  // factory.valRoute($location.path());
  $scope.profile = JSON.parse(window.localStorage.getItem("profile"));
  $scope.token = JSON.parse(window.localStorage.getItem("token"));
  $http.defaults.headers.common['token'] = $scope.token;

  // Pagination
  $scope.pageSize = 10;
  $scope.currentPage=1;
  $scope.pagelengths=[10, 25, 50, 100];
  $scope.limits=[10,25,50,100];

  //Excel options
  $scope.exportData = [];
  $scope.testData = [{ name: "sheet1", data: $scope.exportData }];
  $scope.excel = { down: function() {} };

  $scope.getStock = function() {
    var match = {
      type: "DEPOTSTOCK"
    }
    var data = factory.queryForm(match);
    $http.post('/vehicle/getVehiclesByVariant', data).success(function(data) {
      $scope.depotStockList = data.data;
      // Excel export
      $scope.exportData.push([
        "Sr No",
        "Depot Code",
        "Depot Name",
        "Variant Code",
        "Status",
        "Quantity"
      ]);
      var i = 0;
      angular.forEach($scope.depotStockList, function(value, key) {
        $scope.exportData.push([
          ++i,
          value._id.code,
          value._id.name,
          value._id.variantCode,
          value._id.status,
          value.vehicles
        ]);
      });

    }).error(function(error) {
      $scope.errorMessage = error;
    });
  };
  $scope.getStock();

  $scope.viewStatus = function (depotStock) {
    $scope.variant = depotStock.variantCode;
    $scope.model=depotStock.model;
    $scope.variants = [];
    var obj={};
    obj.chassisNo = depotStock.chassisNo;
      if (depotStock.status == "READY") {
        obj.inTransit = false;
        obj.inStock = true;
        obj.pdi = false;
        obj.damage = false;
      } else if (depotStock.status == "RECEIVED") {
        obj.inTransit = false;
        obj.inStock = false;
        obj.pdi = false;
        obj.damage = true;
      } else if (depotStock.status == "INTRANSIT") {
        obj.inTransit = true;
        obj.inStock = false;
        obj.pid = false; //under damage
        obj.damage = false; //under damage
      } else {
        obj.inTransit = false;
        obj.inStock = false;
        obj.pid = true; //under pdi
        obj.damage = false; //no shotage
      }
      $scope.variants.push(obj);
  };

  // $scope.viewDepotStockStatus = function (depot) {
  //     $http.get('/depot/depotStock_model/'+depot).success(function (data) {
  //         if (data.status == "true") {
  //             $scope.stockStatus = data.msg;
  //             console.log($scope.stockStatus);
  //             return;
  //         }
  //         if (data.status == 400) {
  //             $scope.errorMessage = data.message;
  //             $timeout(callAtTimeout, 3000);
  //             return;
  //         }
  //     }).error(function (error) {
  //         $scope.errorMessage = error;
  //     });
  // };
  /* $scope.depotDetailsById = function (depotId) {
      $http.get('/depot/depotDetailsById?depotId='+depotId).success(function (data) {

          if (data.status == "true") {
              $scope.depotDetail = data.msg;
              console.log($scope.depotDetail);
              return;
          }
          if (data.status == 400) {
              $scope.errorMessage = data.msg;
              $timeout(callAtTimeout, 3000);
              return;
          }
      }).error(function (error) {
          $scope.errorMessage = error;
      });
  }; */

  // if((localStorage.getItem('depotId') !== null || localStorage.getItem('depotId') !==undefined) ){
  //   $scope.depotId=JSON.parse(localStorage.getItem('depotId'));
  //   $scope.depotName=JSON.parse(localStorage.getItem('depot'));
  //    /*  if($scope.depotId) {
  //         $scope.depotDetailsById($scope.depotId);
  //     } */
  //     localStorage.removeItem('depotId');
  //     localStorage.removeItem('depot');
  //     $scope.viewDepotStockStatus($scope.depotId);
  // }
  // else{
  //     $scope.depotId="";
  //     $scope.viewDepotStockStatus($scope.depotId);
  // }



  $scope.viewVehi = function(vari, model) {
    $scope.variants = [];
    $scope.variant = vari[0].variant;
    $scope.model = model;
    var sc = vari.map(function(vehicles) {
      var obj = {};
      obj.chasisNo = vehicles.chasisNo;
      if (vehicles.stock == 1) {
        obj.inTransit = true;
        obj.stock = false;
        obj.pid = true;
        obj.damage = true;
      } else if (vehicles.stock == 2) {
        obj.inTransit = false;
        obj.stock = true;
        obj.pid = false;
        obj.damage = false;
      } else if (vehicles.stock == 3) {
        obj.inTransit = false;
        obj.stock = false;
        obj.pid = false; //under damage
        obj.damage = true; //under damage
      } else {
        obj.inTransit = false;
        obj.stock = false;
        obj.pid = true; //under pdi
        obj.damage = false; //no shotage
      }
      $scope.variants.push(obj);
    })
  }
}]);
