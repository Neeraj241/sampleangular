app.controller('stnReturnReceiptCtrl', ['$rootScope', '$scope', 'factory', '$location', '$http', '$timeout', '$interval', '$routeParams', function ($rootScope,$scope, factory, $location, $http, $timeout, $interval, $routeParams) {
    function callAtTimeout() {
        window.localStorage.removeItem('profile');
        window.localStorage.removeItem('token');
        $location.path('/');

    }
    factory.validatePath();
    $scope.auth = true;
    $scope.profile = JSON.parse(window.localStorage.getItem("profile"));
    $scope.token = JSON.parse(window.localStorage.getItem("token"));
    $http.defaults.headers.common['token'] = $scope.token;

    //Pagination
    $scope.pageSize = 10;
    $scope.currentPage=1;
    $scope.pagelengths=[10, 25, 50, 100];

    $scope.sort = function(keyname){
      $scope.sortKey = keyname;   //set the sortKey to the param passed
      $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    $scope.getDepotList = function () {
        $http.get('/depot/depot/invoices_return', {

        }).success(function (data) {
            if (data.status == "true") {
                // $scope.depotReceiptList = data.data;
                $scope.depotReceiptList = data.msg;
                return;
            }
            if (data.status == 400) {
                $scope.errorMessage = data.message;
                $timeout(callAtTimeout, 3000);
                return;
            }
        }).error(function (error) {
            $scope.errorMessage = error;

        });
    };
    $scope.getDepotList();

    $scope.getDepotReceiptList = function (receipt) {
        receipt.type="return";
        localStorage.setItem("depotReceiptList", JSON.stringify(receipt));
        $location.path('/depotReceiptTransfer');
    };


}]);
