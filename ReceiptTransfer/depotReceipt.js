
app.controller('depotReceiptTransferCtrl', ['$rootScope', '$scope', 'factory', '$location', '$http', '$timeout', '$interval','$window','$filter','Flash','ngToast', function($rootScope, $scope, factory, $location, $http, $timeout, $interval,$window, $filter, Flash,ngToast) {
    factory.validatePath();
  $scope.auth = true;
  $scope.rem={actualTruckNo:"NA",receivedRemarks:"",otherRemarks:""};
  $scope.profile = JSON.parse(window.localStorage.getItem("profile"));
  $scope.token = JSON.parse(window.localStorage.getItem("token"));

  $scope.depotReceiptList = JSON.parse(localStorage.getItem("depotReceiptList"));
        var invoiceNo = {
        invoiceNumber: $scope.depotReceiptList.invoiceNo
      }
  //Pagination
  $scope.pageSize = 10;
  $scope.currentPage=1;
  $scope.pagelengths=[10, 25, 50, 100];
  $scope.sort = function(keyname){
    $scope.sortKey = keyname;   //set the sortKey to the param passed
    $scope.reverse = !$scope.reverse; //if true make it false and vice versa
  }
  $scope.generateDepotReceipt = function() {
    $scope.errorMessage = "";
    $scope.successMessage = "";

    var remarksData = {};
    $scope.depotReceiptLists={};$scope.depotReceiptLists.depot={};$scope.depotReceiptLists.plant={};
      remarksData.invoiceNo= $scope.invoiceNo;
      $scope.depotReceiptLists.status='RECEIVED';
      if($scope.depotReceiptList.type=='return'){
          $scope.depotReceiptLists.status='READY';
      }

        $scope.depotReceiptLists.locationType=$scope.depotReceiptList.vehicles[0].locationType;
        if($scope.depotReceiptLists.locationType=='DEPOT'){
          $scope.depotReceiptLists.depot.recivedDate=new Date();
          if($scope.rem.actualTruckNo=="" || $scope.rem.actualTruckNo==undefined || $scope.rem.actualTruckNo==null){
            $scope.depotReceiptLists.depot.actualTruckNo='NA';
          }else{
            $scope.depotReceiptLists.depot.actualTruckNo=$scope.rem.actualTruckNo;
          }
          $scope.depotReceiptLists.depot.receivedRemarks=$scope.rem.receivedRemarks;
          $scope.depotReceiptLists.depot.receivedRemarks=$scope.rem.receivedRemarks;
          $scope.depotReceiptLists.depot.othersRemarks=$scope.rem.otherRemarks;
        }
        else{
          $scope.depotReceiptLists.plant.recivedDate=new Date();
          $scope.depotReceiptLists.plant.receivedRemarks=$scope.rem.receivedRemarks;
          $scope.depotReceiptLists.plant.othersRemarks=$scope.rem.otherRemarks;
        }
        $scope.depotReceiptLists.updateDate=new Date();
        $scope.depotReceiptLists.action='UPDATE';
        $scope.depotReceiptLists.actionType='RECEIPT';
        $scope.depotReceiptLists.invoiceNumber=$scope.depotReceiptList.invoiceNumber;
        console.log($scope.depotReceiptLists);
        $http.post('/vehicle/receipt', $scope.depotReceiptLists).success(function (data) {
        if (data.status == true) {
          ngToast.create({
            type: 'slide',
            className: 'success',
            content: '<b>Vehicle has been received successfully </b>',
            timeout: 2500
          });
        if($scope.depotReceiptList.type=='return')
            $location.path('/saleReturnReceipt');
        else
          $location.path('/depotDamageList');
        return;
        }
        ngToast.create({
          type: 'slide',
          className: 'danger',
          content: '<b>Unable to receive vehicle.</b>',
          timeout: 3000
        });
      }).error(function(error) {
        ngToast.create({
          type: 'slide',
          className: 'danger',
          content: '<b>Some Technical Error. Please try after some time.</b>',
          timeout: 2500
        });
      });

  };

}]);
